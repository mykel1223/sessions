// [SECTION] Dependencies and Modules
const express = require("express");
const userController = require("../controllers/user");
const auth = require("../auth.js")

// Destructure from auth

const {verify, verifyAdmin} = auth;

// [SECTION] Routing Component
const router = express.Router();

// check email routes
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController));
});

// user registration routes
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// user authentication
router.post("/login", userController.loginUser);


// retrive user details

/*router.post("/details", verify, (req, res) => {
	userController.getProfile(req.body).then(resultFromController => res.send(resultFromController));
})*/

router.get("/details", verify, userController.getProfile);

// Enroll user to a course
router.post("/enroll", verify, userController.enroll);

// Get all enrollments
router.get("/getEnrollments", verify, userController.getEnrollments);

// Reset Password
router.put("/reset-password", verify, userController.resetPassword);


// [SECTION] Export Route System
module.exports = router;

