// [SECTION] Dependencies and Modules
const express = require("express");
const courseController = require("../controllers/Course.js");
const auth = require("../auth.js")

// Destructure from auth

const {verify, verifyAdmin} = auth;

// [SECTION] Routing Component
const router = express.Router();

router.post("/", verify, verifyAdmin, courseController.addCourse);

// Get all courses
router.get("/all", courseController.getAllCourses);

// Get all "active" course
router.get("/", courseController.getAllActive);

// Get a specific course using its ID
router.get("/:courseId", courseController.getCourse);

// Updating a Course (Admin Only)
router.put("/:courseId", verify, verifyAdmin, courseController.updateCourse);

// Archiving a Course (Admin Only)
router.put("/:courseId/archive", verify, verifyAdmin, courseController.archiveCourse);

// Activating a Course (Admin Only)
router.put("/:courseId/activate", verify, verifyAdmin, courseController.activateCourse);

router.post("/archives", verify, verifyAdmin, courseController.archivedCourses);

 

// [SECTION] Export Route System
module.exports = router;
