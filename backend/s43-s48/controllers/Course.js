const Course = require("../models/Course.js");
const bcrypt = require("bcrypt");

const auth = require("../auth.js");

module.exports.addCourse = (req, res) => {
	
	let newCourse = new Course({
		name : req.body.name,
		description : req.body.description,
		price : req.body.price
		
	});

	return newCourse.save().then((course, error) => {
		if(error){
			return res.send(false)
		}else{
			return res.send(true);
		}
	})
	.catch(error => res.send(error));
};

module.exports.getAllCourses = (req, res) => {
	return Course.find({}).then(result => {
		if(result.length === 0){
			return res.send("There is no courses in the DB.");
		}else{
			return res.send(result);
		}
	})
} 

module.exports.getAllActive = (req, res) => {
	return Course.find({isActive : true}).then(result =>{
		if(result.length === 0){
			return res.send("There is currently no active courses")
		}else{
			return res.send(result);
		}
	})
}

module.exports.getCourse = (req, res) => {
	return Course.findById(req.params.courseId).then(result => {
/*		if(result === 0){
			return res.send("Cannot find course with the provided ID.")
		}else{
			return res.send(result);
		}*/

		if(result === 0){
			return res.send("Cannot find course with the provided ID.")
		}else{
			if(result.isActive === false){
				return res.send("The course you are trying to access is not available.");
			}else{
				return res.send(result);
			}
		}
	})
	.catch(error => res.send("Please enter a correct course ID"));
}


module.exports.updateCourse = (req, res) => {
	let updatedCourse = {
		name : req.body.name,
		description : req.body.description,
		price : req.body.price
	}

	return Course.findByIdAndUpdate(req.params.courseId, updatedCourse).then((course, error) => {
		if(error){
			return res.send(false);
		}else{
			return res.send(true);
		}
	});
}

// Archive a course

module.exports.archiveCourse = (req, res) => {
	let updateStatus = {		
		isActive : false
	}

	return Course.findByIdAndUpdate(req.params.courseId, updateStatus).then((course, error) => {
		if(error){
			return res.send(false);
		}else{
			return res.send(true);
		}
	})
	.catch((error) => {
        console.error("Error Archiving course:", error);
        return res.status(500).send(false);
      });
}

// Activate a course

module.exports.activateCourse = (req, res) => {
	let updateStatus = {		
		isActive : true
	};

	Course.findByIdAndUpdate(req.params.courseId, updateStatus).then((course, error) => {
		if(error){
			return res.send(false);
		}else{
			return res.send(true);
		}
	})
	.catch((error) => {
        console.error("Error Activating course:", error);
        return res.status(500).send(false);
      });
}

module.exports.archivedCourses = (req, res) => {
	return Course.find({isActive : false}).then(result => {
		if(result.length === 0){
			return res.send("There is currently no archive courses")
		}else{
			return res.send(result);
		}
	});
	
}



