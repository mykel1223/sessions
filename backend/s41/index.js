const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 3001;

// MongoDB Connection using mongoose

mongoose.connect("mongodb+srv://admin:admin123@cluster0.reijznp.mongodb.net/B305-to-do?retryWrites=true&w=majority", {
	useNewUrlParser : true,
	useUnifiedTopology : true
	// allows us to avoid any current or future errors while connecting to mongoDB
});

// Set notification for connection success or fail
let db = mongoose.connection;
// If error occured, output in console.

db.on("error", console.error.bind(console, "Connection Error!"));

// If connection is successful, output in console
db.once("open", () => console.log("We're connected to the cloud database."));

// [SECTION] Mongoose Schem
// Schemas determine the structure of the documents to be written in the database

const taskSchema = new mongoose.Schema({
	// Define fields with corresponding data type
	name: String,
	status: {
		type: String,
		default: "pending"
		// default -> equips predefined values
	}
})

// [SECTION] Models
const Task = mongoose.model("Task", taskSchema);

// Middlewares
// Allows our app to read json data type
// Allows to read and accept data in form
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// TASK ROUTES

// Business Logic
/*
1. Add a functionality to check if there are duplicate tasks
    - If the task already exists in the database, we return an error
    - If the task doesn't exist in the database, we add it in the database
2. The task data will be coming from the request's body
3. Create a new Task object with a "name" field/property
4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

app.post("/tasks", (req, res) => {

	Task.findOne({name: req.body.name}).then((result, error) => {
		// if document was found or already existing
		if(result !== null && result.name === req.body.name){
			return res.send("Duplicate task found!");
		}else{
			let newTask = new Task({
				name: req.body.name
			})

			newTask.save().then((savedTask, saveErr) => {
				if(saveErr){
					return console.error(saveErr);
				}else{
					// if there is no error
					return res.status(201).send("New Task Created!");
				}
			})
		}
	})
})

// GET ALL TASK
app.get("/tasks", (req, res) => {
	Task.find({}).then((result, error) => {
		// If error occured
		if(error){
			return console.error(error);
		}else{
			return res.status(200).json({
				data: result
			})
		}
	})
})

// ACTIVITY S41

// Create User Schema
const userSchema = new mongoose.Schema({
    // Define fields with corresponding data type
    username: String,
    password: String
})

// Create User Model
const User = mongoose.model("User", userSchema);



// Create User Route
app.post("/signup", (req, res) => {
	console.log(req.body);
    User.findOne({username: req.body.username})
    .then((result, error) => {

        if(result != null && result.username === req.body.username){
            return res.send("Duplicate Username found!");
        }else if(!req.body || !req.body.username || !req.body.password){
            return res.send("BOTH username and password must be provided.");
        }else{
            let NewUser = new User({
                username: req.body.username,
                password: req.body.password
            })

            NewUser.save().then((savedUser, saveErr) => {
                if(saveErr){
                    return console.error(saveErr)
                }else{
                    // if there is no error
                    return res.status(201).send("New User Registered!");
                }
            })

        }
    })

});






// port listening
if(require.main === module){
	app.listen(port, () => console.log(`Server is running at port ${port}`))
}

module.exports = app;