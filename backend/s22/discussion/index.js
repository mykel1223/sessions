// Parameters and Arguments

function printName(name) {
	console.log("My name is " + name);
}

// Calling or Invoking our Function "printName"
// Argument --> Invoke
printName("Juana");
printName("Romenick");

let sampleVariable = "John";

printName(sampleVariable);

// Check Divisibility

function checkDivisibility(num){
	let remainder = num % 8
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);

	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}

// Invocation
checkDivisibility(64);

//Functions as Arguments

function argumentFunction(){
	console.log("This function was passed as an argument.");
}

function invokeFunction(argument){
	argumentFunction();
}

invokeFunction();

// Multiple Parameters in a Function

function createFullName(firstName, middleName, lastName,){
	console.log(firstName + " " + middleName + " " + lastName);
}

// Multiple Arguments in an Invocation
createFullName("Juan", "Dela", "Cruz");
createFullName("Juan", "Dela", "Cruz", "Hello");

// Function with Alert Message
function showSampleAlert(){
	alert("Hello User!");
}

showSampleAlert();

console.log("Testing");

// prompt()

let samplePrompt = prompt("Enter Your Name");

console.log("Hello, " + samplePrompt);

// Function with prompts

function printWelcomeMessage(){
	let firstName = prompt("Enter your First Name");
	let lastName = prompt("Enter your Last Name");

	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to my page!");
}

printWelcomeMessage();

function greeting(){
	console.log("Hi, this is me");
	return "Helo, This is the return statement";
	// code below return statement will never work
	// return -> breaking statement of the function
}

let greetingFunction = greeting();
console.log(greetingFunction);

console.log(greeting());