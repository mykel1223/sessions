

// Get Single To Do [Sample]
async function getSingleToDo(){

   return await (

      
      fetch('https://jsonplaceholder.typicode.com/todos')
      .then((response) => response.json())
      .then((json) => json)
  
  
  )

}



// Getting all to do list item
async function getAllToDo(){

  return await (

   fetch('https://jsonplaceholder.typicode.com/todos')
   .then((response) => response.json())
   .then((json) => console.log(json))



 )

}

// [Section] Getting a specific to do list item
async function getSpecificToDo(){
  
  return await (


     fetch('https://jsonplaceholder.typicode.com/todos/1',
     {
      method: 'GET',
      headers: {
         'Content-type': 'application/json',
      },
      body: JSON.stringify({
         title: 'Created To Do List Item',
         completed: false,
         userId: 1
      })
    }
    )
     .then((response) => response.json())
     .then((json) => console.log(`The item "${json.title}" on the list has a status of ${json.completed}`))



  )

}

// [Section] Creating a to do list item using POST method
async function createToDo(){
  
  return await (

      fetch('https://jsonplaceholder.typicode.com/todos', {
        method: 'POST',
        headers: {
           'Content-type': 'application/json',
        },
        body: JSON.stringify({
           title: 'Created To Do List Item',
           completed: false,
           userId: 1
        })
      })
      .then((response) => response.json())
      .then((json) => console.log(json))



  )

}

// [Section] Updating a to do list item using PUT method
async function updateToDo(){
  
  return await (

      //Add fetch here.
      fetch('https://jsonplaceholder.typicode.com/todos/1', {
        method: 'PUT',
        headers: {
           'Content-type': 'application/json',
        },
           body: JSON.stringify({
           title: 'Updated To Do List Item',
           description: 'To update the my to do list with a different data structure.',
           status: 'Pending',
           dateCompleted: 'Pending',
           userId: 1
        })
      })
      .then((response) => response.json())
      .then((json) => console.log(json))




  );

}

// [Section] Deleting a to do list item
async function deleteToDo(){
  
  return await (

      //Add fetch here.
      fetch('https://jsonplaceholder.typicode.com/todos/1', {
        method: 'DELETE',
      })



  )

}




//Do not modify
//For exporting to test.js
try{
  module.exports = {
      getSingleToDo: typeof getSingleToDo !== 'undefined' ? getSingleToDo : null,
      getAllToDo: typeof getAllToDo !== 'undefined' ? getAllToDo : null,
      getSpecificToDo: typeof getSpecificToDo !== 'undefined' ? getSpecificToDo : null,
      createToDo: typeof createToDo !== 'undefined' ? createToDo : null,
      updateToDo: typeof updateToDo !== 'undefined' ? updateToDo : null,
      deleteToDo: typeof deleteToDo !== 'undefined' ? deleteToDo : null,
  }
} catch(err){

}