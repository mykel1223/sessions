const coursesData = [
	{
		id : "wdc001",
		name : "PHP - Laravel",
		description : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur aliquam justo ut tempor luctus. Nulla lobortis neque sed tincidunt ornare. Nulla egestas fringilla justo, eu tempus felis molestie non. Fusce tincidunt ante gravida enim euismod, non convallis risus ornare. Suspendisse non tristique ante, ac finibus ipsum. Suspendisse potenti. In laoreet ligula tortor, nec auctor massa tristique id. Vestibulum tempor metus in laoreet sollicitudin. Phasellus pharetra ut nisl quis sagittis. Mauris pellentesque magna vel orci volutpat, ac sollicitudin dolor lobortis.",
		price : 45000,
		onOffer : true
	},
	{
		id : "wdc002",
		name : "Pyton - Django",
		description : "Donec id nulla quis arcu tincidunt lobortis. Ut pulvinar, est eget mattis elementum, diam felis pulvinar dolor, non aliquam dolor metus at enim. Sed lobortis nulla et magna aliquam, sit amet placerat sem feugiat. Suspendisse potenti.",
		price : 50000,
		onOffer : true
	},
	{
		id : "wdc003",
		name : "Java - Springboot",
		description : "Nullam sit amet rhoncus turpis, eget cursus nisi. Vestibulum consectetur leo nisl. Sed eleifend neque dui, ac varius ex cursus a. Phasellus quis lectus mauris. Quisque feugiat mi eu lorem luctus cursus. Praesent lacus libero, suscipit in laoreet et, gravida non lectus. Donec vitae metus ac neque euismod pharetra ullamcorper sit amet justo. Phasellus et mauris ac enim luctus vestibulum. Maecenas faucibus aliquet hendrerit.",
		price : 55000,
		onOffer : true
	}
]

export default coursesData;