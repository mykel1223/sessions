import { Navigate } from 'react-router-dom';
// destructures react to use useContext, useEffect, useState without dot notation
import {useContext, useEffect} from 'react';
import UserContext from '../UserContext';

export default function Logout() {

    const {unsetUser, setUser} = useContext(UserContext);
    const {setLogged} = useContext(UserContext);

    // updates localStorage to empty / clears the storage
    unsetUser();

    useEffect(() => {
        // setter function from App.js
        // sets user state saved in context to null
        setUser({
            id : null,
            isAdmin : null
        });
        // setLogged({access: null});
    })

    // localStorage.clear();

    // Redirect back to login
    return (
        <Navigate to='/login' />
    )

}