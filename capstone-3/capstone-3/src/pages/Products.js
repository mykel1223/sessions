import { useEffect, useState, useContext } from 'react';
import ProductCard from '../components/ProductCard';
// import coursesData from '../data/coursesData';
import UserContext from '../UserContext';
import UserView from '../components/UserView';
import AdminView from '../pages/AdminView';

export default function Products() {

    const { user } = useContext(UserContext);

    // Checks to see if the mock data was captured
    // console.log(coursesData);
    // console.log(coursesData[0]);

    // State that will be used to store the courses retrieved from the database
    const [products, setProducts] = useState([]);


    const fetchData = () => {
        fetch(`https://capstone2-santos.onrender.com/product/active-products`)
        .then(res => res.json())
        .then(data => {
            
            console.log(data);

            // Sets the "courses" state to map the data retrieved from the fetch request into several "CourseCard" components
            setProducts(data);

        });
    }


    // Retrieves the courses from the database upon initial render of the "Courses" component
    useEffect(() => {

        fetchData()

    }, []);

    // The "map" method loops through the individual course objects in our array and returns a component for each course
    // Multiple components created through the map method must have a unique key that will help React JS identify which components/elements have been changed, added or removed
    // Everytime the map method loops through the data, it creates a "CourseCard" component and then passes the current element in our coursesData array using the courseProp
    // const courses = coursesData.map(course => {
    //     return (
    //         <CourseCard key={course.id} courseProp={course}/>
    //     );
    // })

    return(
        <>
            {
                (user.isAdmin === true) ?
                    <AdminView productsData={products} fetchData={fetchData} />

                    :

                    <UserView productsData={products} />

            }
        </>
    )
}