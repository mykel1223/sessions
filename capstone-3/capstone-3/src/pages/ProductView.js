import {useState, useEffect, useContext} from 'react';
import {Container, Card, Button, Row, Col} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import "./ProductView.css"


export default function ProductView(){

	const {user} = useContext(UserContext);

	const {productId} = useParams();

	const navigate = useNavigate();


	const [brandName,setBrandName] = useState("");
    const [brandModel,setBrandModel] = useState("");
    const [description,setDescription] = useState("");
	const [img,setImg] = useState("");
    const [price,setPrice] = useState(0);
    const [color,setColor] = useState("");
    const [quantity,setQuantity] = useState(0);

	const product = (productId) => {
		fetch(`https://capstone2-santos.onrender.com/user/order`, {
			method: "POST",
			headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productId: productId
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data.message !== true){
				Swal.fire({
					title: "SUCCESSFUL!",
					icon: "success",
					text: "You have successfully bought this product."
				})

				navigate("/products");
			}else{
				Swal.fire({
					title: "Something Went Wrong!",
					icon: "error",
					text: "Please try again!"
				})
			}
		})
	}

	useEffect(() => {
		console.log(productId);

		fetch(`https://capstone2-santos.onrender.com/product/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);


			setBrandName(data.brandName);
        	setBrandModel(data.brandModel);
        	setDescription(data.description);
			setImg(data.img);
        	setColor(data.color);
        	setQuantity(data.quantity);
        	setPrice(data.price);
		})

	}, [productId])


	return(
		<Container className="mt-5">
            <Row>
                <Col lg={{ span: 6, offset: 3 }}>
                    <Card className="transparent-card" style={{ backgroundColor: 'black', opacity: 0.8, color: 'white' }}>
                        <Card.Body className="text-center d-flex justify-content-center flex-column">
                            <Card.Title>{brandName}</Card.Title>
							<img src={img} style={{ maxWidth: '400px', maxHeight: '400px', marginBottom: '10px' }}/>					
                            <Card.Subtitle>Description:</Card.Subtitle>
							<Card.Subtitle>{brandModel}</Card.Subtitle>
                            <Card.Text> Specification: </Card.Text>
							<Card.Text> {description}</Card.Text>
							<Card.Text>Color: {color}</Card.Text>
							<Card.Text>Quantity: {quantity}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>PhP {price}</Card.Text>
                            

                            {
                            	user.id !== null ?
                            	<Button variant="primary" onClick={() => product(productId)}>Buy Now!</Button>
                            	:
                            	<Link className="btn btn-danger btn-block" to="/login">Login</Link>
                            }

                            
                        </Card.Body>        
                    </Card>
                </Col>
            </Row>
        </Container>
		)
}