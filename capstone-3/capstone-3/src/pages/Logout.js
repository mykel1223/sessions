import { Navigate } from 'react-router-dom';
// destructures react to use useContext, useEffect, useState without dot notation
import {useContext, useEffect} from 'react';
import UserContext from "../UserContext";

export default function Logout() {

    const { unsetUser, setUser } = useContext(UserContext);

    // updates localStorage to empty / clears the storage
    unsetUser();

    useEffect(() => {
        // setter funtion from App.js 
        // sets user state saved in context to null
        setUser({
            id: null,
            isAdmin: null
        });
    })

    // localStorage.clear();

    // Redirect back to login
    return (
        <Navigate to='/' />
    )

}