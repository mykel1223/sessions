import React from 'react';

export const Product = (props) => {
    const {id, productName, description, price} = props.data;
    return (
        <div className="product">
            <div className="description">
                <p>
                    <b>{productName}</b>
                </p>
                <p>
                    <b>{price}</b>
                </p>
            </div>
            <button className="addToCartBttn">Add to Cart</button>
        </div>
    )
}

export default Product;