import {Form, Button, Card, Container, Row, Col} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';
import './Register.css'

export default function Register(){

    
    // State hooks to store values of the input fields
    // Syntax: [stateVariable, setterFunction] = useState("assigned state/initial state")
    // Note: setterFunction is to update the state value

    const {user, setUser} = useContext(UserContext);

    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [mobileNo, setMobileNo] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [isActive, setIsActive] = useState(false);

    
    // const {Logged, setLogged} = useContext(UserContext);


    // Checks if values are successfully binded
    console.log(firstName);
    console.log(lastName);
    console.log(email);
    console.log(mobileNo);
    console.log(password);
    console.log(confirmPassword);

    function registerUser(event){
        
        // Prevents the default behavior during submission
        // which is page redirection via form submission
        event.preventDefault();

        fetch('https://capstone2-santos.onrender.com/user/register',{
            method: "POST",
            headers: {
                "Content-Type" : "application/json"
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName:lastName,
                email: email,
                mobileNo: mobileNo,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            /*data is the response from the web service */
            console.log(data);

            // if the received response is true states will be empty and the use will receive a message "Thank you for registering"
            // else, if not received response is true, the user will recieve a message "Please try again later"
            if(data){

                setFirstName('');
                setLastName('');
                setEmail('');
                setMobileNo('');
                setPassword('');
                setConfirmPassword('');
                localStorage.setItem("user", true);


                setUser({ access: localStorage.getItem('user')})

                alert ('Thank you for registering!');
            }else{
                alert('Please try again later.')
            }
        })
    }

    // The useEffect will run everytime the state changes in any of the field or values listed in
    // [firstName, lastName, email, mobileNo, password, confirmPassword]

    useEffect(() => {

        if((firstName !== "" && 
        lastName !== "" && 
        email !== "" && 
        mobileNo !== "" && 
        password !== "" && 
        confirmPassword !== "") && (password === confirmPassword) && (mobileNo.length === 11)){

            //sets the isActive value to true to enable the button
            setIsActive(true);

        }else{

            setIsActive(false);
        }

    }, [firstName, lastName, email, mobileNo, password, confirmPassword]);

    return (
        (user.id !== null)?
        <Navigate to="/"/>
        :   
        <Container >
            <Row className="justify-content-center align-items-center min-vh-80">
            <Col xs={12} sm={8} md={6}>
        <Card className="transparent-card" style={{
                opacity: 0.8,
                backgroundColor: 'black', // Set the background color to black
                boxShadow: '0 2px 4px rgba(0, 0, 0, 0.986)',               
                color: 'white' // Set shadow style
            }}>
        <Card.Body >
          <Card.Title className="my-5 text-center">Register</Card.Title>

          <Form onSubmit={(event) => registerUser(event)}>
            <Form.Group>
              <Form.Label >First Name:</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter First Name"
                required
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
                className="transparent-input"
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Last Name:</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Last Name"
                required
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
                className="transparent-input"
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Email:</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter Email"
                required
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                className="transparent-input"
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Mobile No:</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter 11 Digit No."
                required
                value={mobileNo}
                onChange={(e) => setMobileNo(e.target.value)}
                className="transparent-input"
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Password:</Form.Label>
              <Form.Control
                type="password"
                placeholder="Enter Password"
                required
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                className="transparent-input"
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Confirm Password:</Form.Label>
              <Form.Control
                type="password"
                placeholder="Confirm Password"
                required
                value={confirmPassword}
                onChange={(e) => setConfirmPassword(e.target.value)}
                className="transparent-input"
              />
            </Form.Group>

            {
                    isActive?
                    <Button variant="danger" type="submit" className="my-3">Submit</Button>
                    :
                    <Button variant="danger" type="submit" className="my-3" disabled>Submit</Button>
                }
          </Form>
        </Card.Body>
      </Card>
      </Col>
  		</Row>
      </Container>
    )
    

} 