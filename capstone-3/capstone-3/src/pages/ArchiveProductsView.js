import React, { useEffect, useState, useContext } from 'react';
import UserContext from '../UserContext';
import UserView from '../components/UserView';
import AdminView from './AdminView';

function ArchiveProducts() {
    const { user } = useContext(UserContext);
    const [archiveProducts, setArchiveProducts] = useState([]);
    let filteredArchiveProducts = []



    const fetchData = () => {
        fetch(`https://capstone2-santos.onrender.com/product/all`)
        .then(res => res.json())
        .then(data => {

            

            data.forEach(archive => {
                if(archive.isActive == false) {
                    filteredArchiveProducts.push(archive);
                    console.log(archive);
                }
                console.log(archive)
            })
            console.log(data);




            console.log(filteredArchiveProducts);
            setArchiveProducts(filteredArchiveProducts);
        });
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
            {user.isAdmin === true ? (
                <AdminView productsData={archiveProducts} fetchData={fetchData} />
            ) : (
                <UserView productsData={archiveProducts} />
            )}
        </>
    );
}

export default ArchiveProducts;

