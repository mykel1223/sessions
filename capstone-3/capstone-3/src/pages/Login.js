import { Form, Button } from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from "../UserContext";
import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2'
import { Container, Row, Col } from 'react-bootstrap';

export default function Login() {

	const {user, setUser} = useContext(UserContext);

	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);

	useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }
    }, [email, password]);

	function authenticate(e) {
		e.preventDefault();
	
		fetch('https://capstone2-santos.onrender.com/user/login', {
			method: 'POST',
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
	
			if (typeof data.access !== "undefined") {
				localStorage.setItem('token', data.access);
	
				// function for retrieving user details
				retrieveUserDetails(data.access);
	
				Swal.fire({
					title: "Login Successful!",
					icon: "success",
					text: "FEEL FREE TO SHOP"
				});
			} else {
				Swal.fire({
					title: "Authentication Failed!",
					icon: "error",
					text: "Check your login details and try again!"
				});
			}
	
			// Clear input fields after submission
			setEmail('');
			setPassword('');
		});
	}

    const retrieveUserDetails = (token) => {
    	fetch(`https://capstone2-santos.onrender.com/user/find-user`, {
    		headers: {
    			Authorization: `Bearer ${token}`
    		}
    	})
    	.then(res => res.json())
    	.then(data => {
    		console.log(data);

    		setUser({
    			id: data._id,
    			isAdmin: data.isAdmin
    		})
    	})
    }


    return (
    	/* if local storage contains token it will direct to courses component using /courses */
    	(user.id !== null)?
    	<Navigate to="/" />
    	: 
    	/* else it will use the form for login */	
		<Container>
  <Row className="justify-content-center align-items-center min-vh-80">
    <Col xs={12} sm={8} md={6}>
	    <Form onSubmit={(e) => authenticate(e)}>
	    	<h1 className="my-5 text-center">Login</h1>
	        <Form.Group controlId="userEmail">
			<Form.Label style={{ color: 'white' }}>Email address</Form.Label>
	            <Form.Control 
	                type="email" 
	                placeholder="Enter email" 
	                required
	                value={email}
	    			onChange={(e) => setEmail(e.target.value)}
					style={{ width: '500px' }}
	            />
	        </Form.Group>
	        <Form.Group controlId="password">
			<Form.Label style={{ color: 'white' }}>Password</Form.Label>
	            <Form.Control 
	                type="password" 
	                placeholder="Password" 
	                required
	                value={password}
	    			onChange={(e) => setPassword(e.target.value)}
					style={{ width: '500px' }}
	            />
	        </Form.Group>

	        <Button variant="danger" type="submit" id="submitBtn" style={{ marginTop: '10px' }}>
				Submit
				</Button>
	    </Form>       
		</Col>
  		</Row>
	</Container>
    )
}