import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home() {

    const data = {
    	/* textual contents*/
        title: "WELCOME TO ALCATRAZ SECRET MARKET",
        content: "GAMING LAPTOPS",
        /* buttons */
        destination: "/products",
        label: "SHOW NOW!"
    }

    return (
        <>
            <Banner data={data}/>
            <Highlights />
        </>
    )
}