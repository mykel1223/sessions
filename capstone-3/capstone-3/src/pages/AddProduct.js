import {useState,useEffect, useContext} from 'react';
import {Form,Button} from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import './AddProduct.css'

export default function AddProduct(){

    const navigate = useNavigate();

    const {user} = useContext(UserContext);

    //input states
    const [brandName,setBrandName] = useState("");
    const [brandModel,setBrandModel] = useState("");
    const [img,setImg] = useState("");
    const [description,setDescription] = useState("");
    const [price,setPrice] = useState("");
    const [color,setColor] = useState("");
    const [quantity,setQuantity] = useState("");
    


    function createProduct(e){

        //prevent submit event's default behavior
        e.preventDefault();

        let token = localStorage.getItem('token');
        console.log(token);

        fetch(`https://capstone2-santos.onrender.com/product/addProduct`,{

            method: 'POST',
            headers: {
                "Content-Type": "application/json",
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({

                brandName: brandName,
                brandModel : brandModel,
                img : img,
                description: description,
                color : color,
                quantity : quantity,
                price: price

            })
        })
        .then(res => res.json())
        .then(data => {

            //data is the response of the api/server after it's been process as JS object through our res.json() method.
            console.log(data);

            if(data){
                Swal.fire({

                    icon:"success",
                    title: "Product Added"

                })

                navigate("/products");
            } else {
                Swal.fire({

                    icon: "error",
                    title: "Unsuccessful Product Creation",
                    text: data.message

                })
            }

        })

        setBrandName("")
        setBrandModel("")
        setImg("")
        setDescription("")
        setColor("")
        setQuantity(0)
        setPrice(0);
    }

    return (

            (user.isAdmin === true)
            ?
            <>
                <h1 className="my-5 text-center">Add Product</h1>
            <div className="form-container">
                <Form onSubmit={e => createProduct(e)}>
                    <Form.Group>
                        <Form.Label>Name:</Form.Label>
                        <Form.Control type="text" placeholder="Enter Name" required value={brandName} onChange={e => {setBrandName(e.target.value)}}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Brand Model:</Form.Label>
                        <Form.Control type="text" placeholder="Enter Brand Model" required value={brandModel} onChange={e => {setBrandModel(e.target.value)}}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Brand Image Link:</Form.Label>
                        <Form.Control type="text" placeholder="Enter Brand Image" required value={img} onChange={e => {setImg(e.target.value)}}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Description:</Form.Label>
                        <Form.Control type="text" placeholder="Enter Description" required value={description} onChange={e => {setDescription(e.target.value)}}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Color:</Form.Label>
                        <Form.Control type="text" placeholder="Enter Color" required value={color} onChange={e => {setColor(e.target.value)}}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Quantity:</Form.Label>
                        <Form.Control type="text" placeholder="Enter Quantity" required value={quantity} onChange={e => {setQuantity(e.target.value)}}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Price:</Form.Label>
                        <Form.Control type="number" placeholder="Enter Price" required value={price} onChange={e => {setPrice(e.target.value)}}/>
                    </Form.Group>
                    <Button variant="danger" type="submit" className="my-5">Submit</Button>
                </Form>
                </div>   
            </>
            
            :
            <Navigate to="/products" />

    )


}