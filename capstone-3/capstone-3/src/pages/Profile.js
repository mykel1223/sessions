import { useState, useEffect, useContext } from 'react';
import { Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext';
import { useNavigate, Navigate } from 'react-router-dom';
import ResetPassword from '../components/ResetPassword';
import './Profile.css';


export default function Profile() {
    const { user } = useContext(UserContext);
    const [details, setDetails] = useState({});

    useEffect(() => {
        fetch(`https://capstone2-santos.onrender.com/user/details`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                // Set the user states values with the user details upon successful login.
                if (typeof data._id !== 'undefined') {
                    setDetails(data);
                }
            });
    }, []);

    return user.id === null ? (
        <Navigate to="/" />
    ) : (
        <>
        <div className="profile-section"> 
            <Row >
                <Col>
                    <h1 className="profile-title">Profile</h1>
                    <h2 className="profile-name">{`${details.firstName || ''} ${details.lastName || ''}`}</h2>
                    <hr className="profile-hr"/>
                    <h4 className="profile-contact">Contacts</h4>
                    <ul>
                        <li>Email: {details.email || ''}</li>
                        <li>Mobile No: {details.mobileNo || ''}</li>
                    </ul>
                </Col>
            </Row>
            <Row className="reset-password-section">
                <Col>
                    <ResetPassword />
                </Col>
            </Row>
        </div>
        </>
    );
}