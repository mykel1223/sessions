import { Row, Col, Card, Carousel } from 'react-bootstrap';
import "./Highlights.css";
import sample from '../assets/sample.jpg'
import sample2 from '../assets/sample2.jpg'

export default function Hightlights(){
	return (

        <div>
      <Carousel>
        <Carousel.Item>
        <img
            className="d-block mx-auto"
            src={sample2}
            alt="Second slide"
            style={{
                opacity: 1,
                boxShadow: '0 2px 4px rgba(0, 0, 0, 0.9)' // Add your shadow style here
              }}
              
          />
          <Carousel.Caption>
            <h1>LENOVO LEGION</h1>
            <p>ON SALE!!</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block mx-auto"
            src={sample}
            alt="Second slide"
            style={{
                opacity: 1,
                boxShadow: '0 2px 4px rgba(0, 0, 0, 0.9)' // Add your shadow style here
              }}
              
          />
          <Carousel.Caption>
            <h1>ROG GAMING LAPTOP</h1>
            <p>ON SALE!!</p>
          </Carousel.Caption>
        </Carousel.Item>
        {/* Add more carousel items as needed */}
      </Carousel>
      
     
       
      <div className="content-container"> 
		<Row className="mt-5 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3" style={{ backgroundColor: 'rgba(0, 0, 0, 0.2)' }}>
			      <Card.Body > 
			        <Card.Title >
			        	<h2>A whole new level gaming experience</h2>
			        </Card.Title>
			        <Card.Text>
			          Some quick example text to build on the card title and make up the
			          bulk of the card's content.
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3" style={{ backgroundColor: 'rgba(0, 0, 0, 0.2)' }}>
			      <Card.Body >
			        <Card.Title>
			        	<h2>Never stop playing</h2>
			        </Card.Title>
			        <Card.Text>
			          Some quick example text to build on the card title and make up the
			          bulk of the card's content.
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3" style={{ backgroundColor: 'rgba(0, 0, 0, 0.2)' }}>
			      <Card.Body>
			        <Card.Title>
			        	<h2>Be a gamer, get a gaming laptop!</h2>
			        </Card.Title>
			        <Card.Text>
			          Some quick example text to build on the card title and make up the
			          bulk of the card's content.
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>
		</Row>
        <footer className="text-center mt-4">
                 <p>&copy; 2023 Your Website Name</p>
             </footer>
        </div>
        </div>
       
 
        
       
        
	)
}