import React, { useState, useContext } from "react"; // Import useState and useEffect
import { Link, NavLink } from "react-router-dom";
import { ShoppingCart } from "phosphor-react";

import "./navbar.css";

import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

import UserContext from '../UserContext';

export const NAVBAR = () => {
  const { user } = useContext(UserContext);

  return (
    <Navbar expand="lg">
      <Container >
        <div className="d-flex">
        {/* <Navbar.Brand className="text-white" as={Link} to="/">ALCATRAZ</Navbar.Brand> */}
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        </div>
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="navbar">
            {/* exact - useful for NavLinks to respond with exact path NOT on partial matches */}
            {/* <Nav.Link className="text-white" as={Link} to="/"> Home </Nav.Link> */}
            <Nav.Link className="text-white" as={Link} to="/"> Home </Nav.Link>
            <Nav.Link className="text-white" as={Link} to="/products"> <ShoppingCart size={32} /> </Nav.Link>
            
            
            {/* <Link to="/cart">               */}
          
            {/* </Link> */}

            {(user.id !== null) ?

            user.isAdmin
             ?
              <>
                <Nav.Link className="text-white" as={Link} to="/addProduct" exact>Add Product</Nav.Link>
                <Nav.Link className="text-white" as={Link} to="/profile" exact>Profile</Nav.Link>
                <Nav.Link className="text-white" as={Link} to="/logout" exact>Logout</Nav.Link>
                <Nav.Link className="text-white" as={NavLink} to="/archive" exact>Archive</Nav.Link>
              </>
              :
              <>
                <Nav.Link className="text-white" as={Link} to="/profile" exact>Profile</Nav.Link>
                <Nav.Link className="text-white" as={Link} to="/logout" exact>Logout</Nav.Link>
              </>
              :
              <>
                <Nav.Link className="text-white" as={Link} to="/login" exact>Login</Nav.Link>
                <Nav.Link className="text-white" as={Link} to="/register" exact>Register</Nav.Link>
              </>
            }

          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default NAVBAR;