import { Card, Button } from 'react-bootstrap';
import products from '../productsData';
import {useState} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import "./ProductCard.css"
import { Divide } from 'phosphor-react';

export default function ProductCard({productProp}) {
	// Checks to see if the data was successfully passed
	//console.log(props);
	// Every component recieves information in a form of an object
	//console.log(typeof props);

	const { _id, brandName, brandModel, img, description, color, quantity, price } = productProp;

	return (
    
    <div className="product">
      <Link to={`/product/${_id}`} className="link-without-underline">
      <div className="description" >
        
          <h1>
            <b>{brandName}</b>
            
            
          </h1>
          <img src={img} style={{ maxWidth: '100px', maxHeight: '100px' }}/>
          <p>{brandModel}</p>
          <p>{color}</p>
          <p>PhP {price}</p>
          {/* <Link className="addToCartBttn" >Details</Link> */}
        </div>
      </Link>

        


	{/* <Card>
	    <Card.Body >
	        <Card.Title>{brandName}</Card.Title>
          <Card.Subtitle>{brandModel}</Card.Subtitle>
	        <Card.Subtitle>Description:</Card.Subtitle>
	        <Card.Text>{description}</Card.Text>
          <Card.Text>{color}</Card.Text>
          <Card.Text>{quantity}</Card.Text>
	        <Card.Subtitle>Price:</Card.Subtitle>
	        <Card.Text>PhP {price}</Card.Text>
	        <Link className="btn btn-primary" to={`/product/${_id}`}>Details</Link>
	    </Card.Body>
	</Card>	 */}
  </div>
  


	)
}

// Check if the productCard component is getting the correct prop types
ProductCard.propTypes = {
	product: PropTypes.shape({
		brandName: PropTypes.string.isRequired,
    brandModel: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
    color: PropTypes.string.isRequired,
    quantity: PropTypes.number.isRequired,
		price: PropTypes.number.isRequired
	})
}