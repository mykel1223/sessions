import React, { useState, useEffect } from 'react';
import ProductCard from './ProductCard';
// import CourseSearch from './CourseSearch';

export default function UserView({productsData}) {

    const [products, setProducts] = useState([])

    useEffect(() => {
        const productsArr = productsData.map(products => {
            //only render the active courses
            if(products.isActive === true) {
                return (
                    <ProductCard productProp={products} key={products._id}/>
                    )
            } else {
                return null;
            }
        })

        // set the courses state to the result of our map function, to bring our returned course component outside of the scope of our useEffect where our return statement below can see.
        setProducts(productsArr)
        
        

    }, [productsData])

    return(
        <>
            {/* <CourseSearch /> */}
            { products }
        </>
        )
}