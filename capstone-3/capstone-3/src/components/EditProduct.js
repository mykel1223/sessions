import {Button, Modal, Form} from 'react-bootstrap';
import {useState} from 'react';
import Swal from 'sweetalert2';

export default function EditProduct({productData, fetchData}){
	console.log(productData);

	// state for product id which will be used for fetching
	const [productId, setProductId] = useState("");

	// state for editproduct modal
	const [showEdit, setShowEdit] = useState(false);

	// useState for our form (modal)
	const [brandName, setBrandName] = useState("");
	const [brandModel, setBrandModel] = useState("");
	const [img, setImg] = useState("");
	const [description, setDescription] = useState("");
	const [color, setColor] = useState("");
	const [quantity, setQuantity] = useState(0);
	const [price, setPrice] = useState(0);

	// function for opening the edit modal
		
	const openEdit = (productId) => {
		setShowEdit(true);

		// to still get the actual data from the form
		fetch(`https://capstone2-santos.onrender.com/product/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setProductId(data._id);
			setBrandName(data.brandName);
			setImg(data.img);
			setBrandModel(data.brandModel);
			setDescription(data.description);
			setColor(data.color);
			setQuantity(data.quantity);
			setPrice(data.price);
		})
	}

	const closeEdit = () => {
		setShowEdit(false);
		setBrandName("");
		setImg("");
		setBrandModel("");
		setDescription("");
		setColor("");
		setQuantity(0);
		setPrice(0);
	}

	// function to save our update
	const editProduct = (e, productId) => {
		e.preventDefault();

		fetch(`https://capstone2-santos.onrender.com/product/${productId}`, {
			method: "PUT",
			headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				brandName: brandName,
				brandModel: brandModel,
				img: img,
				description: description,
				color: color,
				quantity: quantity,
				price: price
			})
			})
		.then(res => res.json())
		.then(data => {
			
			console.log(data);

			if(data){
				Swal.fire({
					title: "Update Success!",
					icon: "success",
					text: "Product Successfully Updated!"
				})
				fetchData();
				closeEdit();
			}else{
				Swal.fire({
					title: "Update Error!",
					icon: "error",
					text: "Please try again!"
				})
				closeEdit();
			}

		})
	}

	return(
		<>
			<Button variant="primary" size="sm" onClick={() => {openEdit(productData)}}>Edit</Button>

			 <Modal show={showEdit} onHide={closeEdit}>
                <Form onSubmit={e => editProduct(e, productId)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Edit Product</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>    
                        <Form.Group controlId="productName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control type="text" required value={brandName} onChange={e => setBrandName(e.target.value)}/>
                        </Form.Group>
						<Form.Group controlId="productModel">
                            <Form.Label>Model</Form.Label>
                            <Form.Control type="text" required value={brandModel} onChange={e => setBrandModel(e.target.value)}/>
                        </Form.Group>
						<Form.Group controlId="productImg">
                            <Form.Label>Image</Form.Label>
                            <Form.Control type="text" required value={img} onChange={e => setImg(e.target.value)}/>
                        </Form.Group>
                        <Form.Group controlId="productDescription">
                            <Form.Label>Description</Form.Label>
                            <Form.Control type="text" required value={description} onChange={e => setDescription(e.target.value)}/>
                        </Form.Group>
						<Form.Group controlId="productColor">
                            <Form.Label>Color</Form.Label>
                            <Form.Control type="text" required value={color} onChange={e => setColor(e.target.value)}/>
                        </Form.Group>
						<Form.Group controlId="productQuantity">
                            <Form.Label>Quantity</Form.Label>
                            <Form.Control type="text" required value={quantity} onChange={e => setQuantity(e.target.value)}/>
                        </Form.Group>
                        <Form.Group controlId="productPrice">
                            <Form.Label>Price</Form.Label>
                            <Form.Control type="number" required value={price} onChange={e => setPrice(e.target.value)}/>
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={closeEdit}>Close</Button>
                        <Button variant="success" type="submit">Submit</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
		</>

		)
}