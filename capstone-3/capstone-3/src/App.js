import "./App.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Navbar from './components/navbar';
// import Shop from "./pages/shop/shop";
// import Contact from "./pages/contact";
// import Cart from "./pages/cart/cart";
// import ShopContextProvider from "./context/shop-context";
import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext';

import Register from './pages/Register';
import Profile from './pages/Profile';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Products from './pages/Products';
import ProductView from './pages/ProductView';
import AddProduct from './pages/AddProduct';
import Home from './pages/Home';
import Main from './components/Main';
import ArchiveProductsView from './pages/ArchiveProductsView'


function App() {

    const [user, setUser] = useState({
        id: null,
        isAdmin: null
      });
    
        // Function for clearing localStorage on logout
        const unsetUser = () => {
    
          localStorage.clear();
    
        };
    
        //Because our user state's values are reset to null every time the user reloads the page (thus logging the user out), we want to use React's useEffect hook to fetch the logged-in user's details when the page is reloaded. By using the token saved in localStorage when a user logs in, we can fetch the their data from the database, and re-set the user state values back to the user's details.
        useEffect(() => {
    
        // console.log(user);
        fetch(`https://capstone2-santos.onrender.com/user/details`, {
          headers: {
            Authorization: `Bearer ${ localStorage.getItem('token') }`
          }
        })
        .then(res => res.json())
        .then(data => {
          console.log(data)
          // Set the user states values with the user details upon successful login.
          if (typeof data._id !== "undefined") {
    
            setUser({
              id: data._id,
              isAdmin: data.isAdmin
            });
    
          // Else set the user states to the initial values
          } else {
    
            setUser({
              id: null,
              isAdmin: null
            });
    
          }
    
        })
    
        }, []);

  return (
    
    <UserProvider value={{user, setUser, unsetUser}}>
      <div>
    <div className="App">
      {/* <ShopContextProvider> */}
      <Main />
        <Router>
          <Navbar />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/products" element={<Products />} />
            <Route path="/product/:productId" element={<ProductView />}/>
            {/* <Route path="/contact" element={<Contact />} /> */}
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            {/* <Route path="/cart" element={<Cart />} /> */}
            <Route path="/register" element={<Register />} />
            <Route path="/profile" element={<Profile />} />
            <Route path="/addProduct" element={<AddProduct />} />
            <Route path="/archive" element={<ArchiveProductsView />} />
            </Routes>
        </Router>
      {/* </ShopContextProvider> */}
    </div>
    
    </div>
    </UserProvider>
  );
}

export default App;