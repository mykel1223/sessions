// [SECTION] Dependencies and Modules
const express = require("express");
const userController = require("../controllers/User.js");
const auth = require("../Auth.js")

// Destructure from auth

const {verify, verifyAdmin} = auth;

// [SECTION] Routing Component
const router = express.Router();

// user registration routes
// router.post("/registration", userController.registerUser);

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// user authentication
router.post("/login", userController.loginUser);

// Reset Password
router.put('/reset-password', verify, userController.resetPassword);

// get user details
router.get("/find-user", verify, userController.getUser);

 // setting a user as admin
// New endpoint for setting a user as an admin
router.put("/:userId/set-admin", verify, verifyAdmin, userController.setAdmin);

// Order a product
router.post("/order", verify, userController.orderProduct);

// // getting order list
router.get("/orderList", verify, userController.getOrderedProducts);

// deleting a order
router.delete('/:orderId/deleteOrder', verify, userController.deleteOrderedProduct);

//check out
router.post('/checkout', verify, userController.checkout);

// get all orders
router.get('/allOrders', verify, verifyAdmin, userController.getAllOrderedProducts);


// get all orders
router.get('/history', verify, verifyAdmin, userController.getOrderHistory);

//[ACTIVITY] Route for retrieving user details
router.get("/details", verify, userController.getProfile);
// [SECTION] Export Route System
module.exports = router;