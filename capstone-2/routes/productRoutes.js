// [SECTION] Dependencies and Modules
const express = require("express");
const productController = require("../controllers/productControllers.js");
const auth = require("../Auth.js")

// Destructure from auth

const {verify, verifyAdmin} = auth;

// [SECTION] Routing Component
const router = express.Router();

router.post("/addProduct", verify, verifyAdmin, productController.addProduct);

// // Get all product
router.get("/all", productController.getAllProduct);

// // Get all "active" course
router.get("/active-products", productController.getAllActiveProduct);

// Get a specific product using its ID
router.get("/:productId", productController.getProduct);

// // Updating a Product (Admin Only)
router.put("/:productId", productController.updateProduct);

// // Archiving a product (Admin Only)
router.put("/:productId/archive", verify, verifyAdmin, productController.archiveProduct);

// // Activating a product (Admin Only)
router.put("/:productId/activate", verify, verifyAdmin, productController.activateProduct);

// get all archive products
router.put("/:productId/archive-products", productController.archiveProduct);

// get all archive products
router.post("/archive-products", verify, verifyAdmin, productController.archivedProducts);

 



// [SECTION] Export Route System
module.exports = router;