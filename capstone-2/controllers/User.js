// [SECTION] Dependencies and Modules
const User = require("../models/User.js");
const Product = require("../models/productModels.js");
const bcrypt = require("bcrypt");
const auth = require("../Auth.js");


// user registration 

module.exports.registerUser = async (reqBody) => {
  
  try {
    console.log(reqBody)
    // Check if the email and password properties exist in reqBody
    if (!reqBody.email || !reqBody.password) {
      return { success: false, message: 'Email and password are required' };
    }

    // Check if the email already exists in the database
    const existingUser = await User.findOne({ email: reqBody.email });

    if (existingUser) {
      // Email already exists
      return { success: false, message: 'Email already registered' };
    }

    // Create a new user
    const newUser = new User({
      firstName: reqBody.firstName,
      lastName: reqBody.lastName,
      email: reqBody.email,
      mobileNo: reqBody.mobileNo,
      password: bcrypt.hashSync(reqBody.password, 10)
    });

    // Save the new user to the database
    console.log(newUser);
    await newUser.save();

    return { success: true, message: 'User registered successfully' };
  } catch (error) {
    // Handle any errors, e.g., log the error
    console.error(error);
    return { success: false, message: 'Registration failed' };
  }
};


// user authentication

module.exports.loginUser = (req, res) => {
  User.findOne({ email: req.body.email })
    .then((result) => {
      if (result === null) {
        return res.status(401).json({ error: 'Email not found' });
      } else {
        const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

        if (isPasswordCorrect) {
          // Authentication successful

          // Check the user's role
          if (result.isAdmin === true) {
            // Admin user
            const accessToken = auth.createAccessToken(result);
            return res.status(200).json({ access: accessToken, role: 'You logged on as admin.' });
          } else {
            // Regular user
            const accessToken = auth.createAccessToken(result);
            return res.status(200).json({ access: accessToken, role: 'You logged on as client.' });
          }
        } else {
          // Password is incorrect
          return res.status(401).json({ error: 'Incorrect password' });
        }
      }
    })
    .catch((err) => {
      console.error(err);
      return res.status(500).json({ error: 'Internal server error' });
    });
};

// user change password

module.exports.resetPassword = async (req, res) => {
        try {

        //console.log(req.user)
        //console.log(req.body)

          const { newPassword } = req.body;
          const { id } = req.user; // Extracting user ID from the authorization header
      
          // Hashing the new password
          const hashedPassword = await bcrypt.hash(newPassword, 10);
      
          // Updating the user's password in the database
          await User.findByIdAndUpdate(id, { password: hashedPassword });
      
          // Sending a success response
          res.status(200).send({ message: 'Password reset successfully' });
        } catch (error) {
          console.error(error);
          res.status(500).send({ message: 'Internal server error' });
        }
    };

// Find a user via Email

module.exports.getUser = (req, res) => {
  // const userEmail = req.body.email; // Get the email from the request body

  // // Find the user by email
  // User.findOne({ email: userEmail })
  //   .then((user) => {
  //     if (!user) {
  //       return res.status(404).json({ message: 'User not found' });
  //     }

  //     res.status(200).json(user);
  //   })
  //   .catch((error) => {
  //     console.error(error);
  //     res.status(500).json({ message: 'Internal Server Error' });
  //   });
  return User.findById(req.user.id).then(result => {
    result.password = "";
    return res.send(result);
  })
  .catch(error => error)
};

// setting a user as a admin

module.exports.setAdmin = async (req, res) => {
  try {
    const userId = req.params.userId;

    // Check if the user making the request is an admin (you may implement authentication and authorization for this)
    if (!req.user.isAdmin) {
      return res.status(403).json({ message: 'Permission denied. Only admins can perform this action.' });
    }

    // Find the user by their ID
    const user = await User.findById(userId);

    if (!user) {
      return res.status(404).json({ message: 'User not found. No user with the specified ID.' });
    }

    // Check if the user is already an admin
    if (user.isAdmin) {
      return res.status(200).json({ message: 'User is already an admin.' });
    }

    // Update the isAdmin field to true
    user.isAdmin = true;
    await user.save();

    res.status(200).json({ message: 'User set as admin successfully' });
  } catch (error) {
    console.error('Error setting user as admin:', error);
    res.status(500).json({ message: 'Internal Server Error' });
  }
};



// product order

module.exports.orderProduct = async (req, res) => {
  if (req.user.isAdmin) {
    return res.send("You cannot do this action.");
  }

  try {
    // Find the user by ID
    const user = await User.findById(req.user.id);

    if (!user) {
      return res.status(404).send("User not found.");
    }

    // Find the product by ID
    const product = await Product.findById(req.body.productId);

    if (!product) {
      return res.status(404).send("Product not found.");
    }

    // Check if the product is active
    if (!product.isActive) {
      return res.send("This product is not currently available for ordering.");
    }

    // Check if the product is in stock
    if (product.quantity > 0) {
      // Reduce the quantity of the product
      product.quantity -= 1;

      // Save the updated product
      await product.save();

      // Create a new ordered product
      const newOrderedProduct = {
        productId: req.body.productId,
        // You can include other order details here if needed
      };

      // Add the ordered product to the user's orders list
      user.userOrders.push(newOrderedProduct);

      // Save the updated user
      await user.save();

      // Prepare the ordered product details
      const orderedProductDetails = {
        productId: product._id,
        brandName: product.brandName,
        brandModel: product.brandModel,
        description: product.description,
        img : req.body.img,
        color: product.color,
        price: product.price,
        // Include other product details here
      };

      return res.json({
        message: "You successfully ordered this product. Thank you",
        orderedProduct: orderedProductDetails,
      });
    } else {
      return res.send("Product out of stock.");
    }
  } catch (error) {
    console.error('Error ordering product:', error);
    return res.status(500).json({ message: 'Internal Server Error' });
  }
};


// get ordered product list with total price

module.exports.getOrderedProducts = async (req, res) => {
  if (req.user.isAdmin) {
    return res.send("You cannot do this action.");
  }
  try {
    const userId = req.user.id;

    // Find the user by ID and populate the 'userOrders' array with product details, including the 'price' field
    const user = await User.findById(userId).populate({
      path: 'userOrders.productId',
      select: 'brandName brandModel description color price', // Select the fields you want to populate
    });

    if (!user || !user.userOrders || user.userOrders.length === 0) {
      return res.send("You don't have any orders yet.");
    }

    // Extract the product details from the populated 'userOrders'
    const orderedProducts = user.userOrders.map((order) => ({
      productId: order.productId._id,
      brandName: order.productId.brandName,
      brandModel: order.productId.brandModel,
      description: order.productId.description,
      img : req.body.img,
      color: order.productId.color,
      price: order.productId.price,
      OrderedOn: order.OrderedOn,
      status: order.status,
    }));

    // Calculate the total price
    const totalPrice = orderedProducts.reduce((total, product) => total + product.price, 0);

    res.send({ orderedProducts, totalPrice });
  } catch (error) {
    console.error('Error getting ordered products:', error);
    return res.status(500).json({ message: 'Internal Server Error' });
  }
};

// deleting ordered product

module.exports.deleteOrderedProduct = async (req, res) => {
  if (req.user.isAdmin) {
    return res.send("You cannot do this action.");
  }
  try {
    const userId = req.user.id;
    const orderId = req.params.orderId; // Get the orderId from the request params

    // Find the user by ID
    const user = await User.findById(userId);

    if (!user) {
      return res.status(404).send("User not found.");
    }

    // Find the ordered product by its ID in the user's userOrders array
    const orderedProductIndex = user.userOrders.findIndex(order => order._id.toString() === orderId);

    if (orderedProductIndex === -1) {
      return res.status(404).send("Ordered product not found.");
    }

    // Retrieve the ordered product details before removing it
    const orderedProduct = user.userOrders[orderedProductIndex];

    // Remove the ordered product from the user's userOrders array
    user.userOrders.splice(orderedProductIndex, 1);

    // Save the updated user
    await user.save();

    // Update the product's quantity (if needed) and save the product
    const product = await Product.findById(orderedProduct.productId);

    if (product) {
      product.quantity += 1; // Increase the quantity since the product is no longer ordered
      await product.save();
    }

    return res.send(`Ordered product with ID ${orderId} has been deleted.`);
  } catch (error) {
    console.error('Error deleting ordered product:', error);
    return res.status(500).json({ message: 'Internal Server Error' });
  }
};

// checkout 

module.exports.checkout = async (req, res) => {
  if (req.user.isAdmin) {
    return res.send("You cannot do this action.");
  }
  try {
    const userId = req.user.id;

    // Find the user by ID
    const user = await User.findById(userId);

    if (!user) {
      return res.status(404).send("User not found.");
    }

    // Check if the user has any ordered products
    if (user.userOrders.length === 0) {
      return res.send("You don't have any ordered products to checkout.");
    }

    // Collect information about ordered products and calculate the total price
    const orderedProductDetails = [];
    let totalPrice = 0; // Initialize the total price

    for (const orderedProduct of user.userOrders) {
      const product = await Product.findById(orderedProduct.productId);

      if (!product) {
        // Handle the case where a product is not found
        continue;
      }

      // Include details of the ordered product
      const productDetails = {
        productId: product._id,
        brandName: product.brandName,
        brandModel: product.brandModel,
        description: product.description,
        img : req.body.img,
        color: product.color,
        price: product.price,
        // Include other product details here
      };

      orderedProductDetails.push(productDetails);

      // Add the product's price to the total price
      totalPrice += product.price;
    }

    // Perform the checkout process here
    // You can create an order record, update payment status, etc.

    // Clear the user's userOrders array after checkout
    user.userOrders = [];

    // Save the updated user
    await user.save();

    // Return ordered product details, total price, and checkout success message
    return res.json({
      message: "Checkout successful. Your ordered products have been processed.",
      orderedProducts: orderedProductDetails,
      totalPrice: totalPrice, // Include the total price in the response
    });
  } catch (error) {
    console.error('Error during checkout:', error);
    return res.status(500).json({ message: 'Internal Server Error' });
  }
};

// getting all orders

module.exports.getAllOrderedProducts = async (req, res) => {
  try {
    // Fetch all users with their populated 'userOrders' array
    const users = await User.find().populate({
      path: 'userOrders.productId',
      select: 'brandName brandModel description color price', // Select the fields you want to populate
    });

    const allOrderedProducts = [];

    // Extract ordered products from each user
    users.forEach((user) => {
      if (user.userOrders && user.userOrders.length > 0) {
        const userOrderedProducts = user.userOrders.map((order) => ({
          productId: order.productId._id,
          firstName : user.firstName,
          lastName : user.lastName,
          brandName: order.productId.brandName,
          brandModel: order.productId.brandModel,
          description: order.productId.description,
          color: order.productId.color,
          price: order.productId.price,
          OrderedOn: order.OrderedOn,
          status: order.status,
        }));

        allOrderedProducts.push(...userOrderedProducts);
      }
    });

    // Calculate the total price for all ordered products
    const totalPrice = allOrderedProducts.reduce((total, product) => total + product.price, 0);

    res.send({ orderedProducts: allOrderedProducts, totalPrice });
  } catch (error) {
    console.error('Error getting all ordered products:', error);
    return res.status(500).json({ message: 'Internal Server Error' });
  }
};

// get all ordered products history

module.exports.getOrderHistory = async (req, res) => {
  if (!req.user.isAdmin) {
    return res.status(403).json({ message: 'You do not have permission to access this data.' });
  }

  try {
    // Find all users and populate their 'userOrders' array with product details
    const users = await User.find().populate({
      path: 'userOrders.productId',
      select: 'brandName brandModel description color price',
    });

    if (!users || users.length === 0) {
      return res.send("There are no orders for any users.");
    }

    // Create an array to store all ordered products from all users
    const allOrderedProducts = [];

    // Iterate through each user and extract their ordered products
    users.forEach((user) => {
      if (user.userOrders && user.userOrders.length > 0) {
        const orderedProducts = user.userOrders.map((order) => ({
          userId: user._id,
          productId: order.productId._id,
          firstName : user.firstName,
          lastName : user.lastName,
          brandName: order.productId.brandName,
          brandModel: order.productId.brandModel,
          description: order.productId.description,
          color: order.productId.color,
          price: order.productId.price,
          OrderedOn: order.OrderedOn,
          status: order.status,
        }));

        allOrderedProducts.push(...orderedProducts);
      }
    });

    // Calculate the total price for all ordered products
    const totalPrice = allOrderedProducts.reduce((total, product) => total + product.price, 0);

    res.send({ allOrderedProducts, totalPrice });
  } catch (error) {
    console.error('Error getting all ordered products:', error);
    return res.status(500).json({ message: 'Internal Server Error' });
  }
};

module.exports.getProfile = (req, res) => {

        return User.findById(req.user.id)
        .then(result => {
            result.password = "";
            return res.send(result);
        })
        .catch(err => res.send(err))
    };

