const Product = require("../models/productModels.js");
const bcrypt = require("bcrypt");

const auth = require("../Auth.js");

// add product

// module.exports.addProduct = async (req, res) => {
//   // Extract data from the request body
//   const { brandName, brandModel, description, color, price } = req.body;

//   try {
//     // Check for existing products with the same brandName, brandModel, and description
//     const existingProduct = await Product.findOne({ brandName, brandModel, description });

//     if (existingProduct) {
//       return res.status(400).json({ success: false, message: 'Product with the same details already exists' });
//     }

//     // Create a new product
//     const newProduct = new Product({
//       brandName,
//       brandModel,
//       description,
//       color,
//       price,
//     });

//     // Save the new product to the database
//     await newProduct.save();

//     return res.status(201).json({ success: true, message: 'Product successfully added' });
//   } catch (error) {
//     console.error('Error adding product:', error);
//     return res.status(500).json({ success: false, message: 'Failed to add product' });
//   }
// };

 module.exports.addProduct = (req, res) => {

  

        const newProduct = new Product({
          brandName : req.body.brandName,
          brandModel : req.body.brandModel,
          img : req.body.img,
          description : req.body.description,
          color : req.body.color,
          quantity : req.body.quantity,
          price : req.body.price
     });

        return newProduct.save().then((product, error) => {
            // Course creation successful
            if (error) {
                return res.send(false);

            // Course creation failed
            } else {
                return res.send(true);
            }
        })
        .catch(err => res.send(err))
    }

// get all product

module.exports.getAllProduct = (req, res) => {
	return Product.find({}).then(result => {
		if(result.length === 0){
			return res.send("There is no product in the DB.");
		}else{
			return res.send(result);
		}
	})
}

// get all active product

module.exports.getAllActiveProduct = (req, res) => {
	return Product.find({isActive : true}).then(result =>{
		if(result.length === 0){
			return res.send("There is currently no active product")
		}else{
			return res.send(result);
		}
	})
}

// get single product

module.exports.getProduct = (req, res) => {
	return Product.findById(req.params.productId).then(result => {

		if(result === 0){
			return res.send("Cannot find product with the provided ID.")
		}else{
			if(result.isActive === false){
				return res.send("The product you are trying to access is not available.");
			}else{
				return res.send(result);
			}
		}
	})
	.catch(error => res.send("Please enter a correct product ID"));
}

// update a product

// module.exports.updateProduct = async (req, res) => {
//   const updatedProduct = {
  //   brandName: req.body.brandName,
  //   brandModel: req.body.brandModel,
  //   description: req.body.description,
  //   color: req.body.color,
  //   quantity: req.body.quantity,
  //   price: req.body.price,
  // };

//   try {
//     const productId = req.params.productId;

//     // Find the existing product by ID
//     const existingProduct = await Product.findById(productId);

//     if (!existingProduct) {
//       return res.status(404).json({ success: false, message: 'Product not found' });
//     }

//     // Check if the updated values are the same as the existing product's values
//     if (
//       updatedProduct.brandName === existingProduct.brandName &&
//       updatedProduct.brandModel === existingProduct.brandModel &&
//       updatedProduct.description === existingProduct.description &&
//       updatedProduct.color === existingProduct.color &&
//       updatedProduct.quantity === existingProduct.quantity &&
//       updatedProduct.price === existingProduct.price
//     ) {
//       return res.status(200).json({ success: true, message: 'Product already updated' });
//     }

//     // Update the product
//     const updated = await Product.findByIdAndUpdate(productId, updatedProduct);

//     if (updated) {
//       return res.status(200).json({ success: true, message: 'Update Successful' });
//     } else {
//       return res.status(500).json({ success: false, message: 'Failed to update product' });
//     }
//   } catch (error) {
//     console.error('Error updating product:', error);
//     return res.status(500).json({ success: false, message: 'Failed to update product' });
//   }
// };

module.exports.updateProduct = (req, res) => {
            // Specify the fields/properties of the document to be updated
            const updatedProduct = {
    brandName: req.body.brandName,
    brandModel: req.body.brandModel,
    description: req.body.description,
    img : req.body.img,
    color: req.body.color,
    quantity: req.body.quantity,
    price: req.body.price,
  };

            // Syntax
                // findByIdAndUpdate(document ID, updatesToBeApplied)
            return Product.findByIdAndUpdate(req.params.productId, updatedProduct).then((product, error) => {

                // Course not updated
                if (error) {
                    return res.send(false);

                // Course updated successfully
                } else {                
                    return res.send(true);
                }
            })
            .catch(err => res.send(err))
        };

// archive a product

// module.exports.archiveProduct = async (req, res) => {
//   const updateStatus = {
//     isActive: false,
//   };

//   try {
//     const productId = req.params.productId;

//     // Find the existing product by ID
//     const existingProduct = await Product.findById(productId);

//     if (!existingProduct) {
//       return res.status(404).json({ success: false, message: 'Product not found' });
//     }

//     // Check if the product is already archived
//     if (!existingProduct.isActive) {
//       return res.status(200).json({ success: true, message: 'Product is already in archives' });
//     }

//     // Update the product's isActive status
//     const updated = await Product.findByIdAndUpdate(productId, updateStatus);

//     if (updated) {
//       return res.status(200).json({ success: true, message: 'Product has been archived' });
//     } else {
//       return res.status(500).json({ success: false, message: 'Failed to archive product' });
//     }
//   } catch (error) {
//     console.error('Error archiving a product:', error);
//     return res.status(500).json({ success: false, message: 'Failed to archive product, Please check the product ID' });
//   }
// };

  module.exports.archiveProduct = (req, res) => {

        let updateActiveField = {
            isActive: false
        }

        return Product.findByIdAndUpdate(req.params.productId, updateActiveField)
        .then((product, error) => {

            //product archived successfully
            if(error){
                return res.send(false)

            // failed
            } else {
                return res.send(true)
            }
        })
        .catch(err => res.send(err))

    };

// activate a product

module.exports.activateProduct = async (req, res) => {
  const updateStatus = {
    isActive: true, // Set isActive to true to activate the product
  };

  try {
    const productId = req.params.productId;

    // Find the existing product by ID
    const existingProduct = await Product.findById(productId);

    if (!existingProduct) {
      return res.status(404).json({ success: false, message: 'Product not found' });
    }

    // Check if the product is already active
    if (existingProduct.isActive) {
      return res.status(200).json({ success: true, message: 'Product is already active' });
    }

    // Update the product's isActive status to true (activate it)
    const updated = await Product.findByIdAndUpdate(productId, updateStatus);

    if (updated) {
      return res.send(true)
    } else {
      return res.status(500).json({ success: false, message: 'Failed to activate product' });
    }
  } catch (error) {
    console.error('Error activating a product:', error);
    return res.status(500).json({ success: false, message: 'Failed to activate product, Please check the product ID' });
  }
};

// getting all archived products

module.exports.archivedProducts = async (req, res) => {
  try {
    // Find all archived products (products with isActive: false)
    const archivedProducts = await Product.find({ isActive: false });

    if (archivedProducts.length === 0) {
      return res.send("There is currently no archive products.");
    } else {
      // Extract and return specific details for each archived product
      const archivedProductDetails = archivedProducts.map((product) => ({
        productId: product._id,
        brandName: product.brandName,
        description: product.description,
        img : req.body.img,
        price: product.price,
        isActive : product.isActive,
        // Include other product details as needed
      }));

      return res.json(archivedProductDetails);
    }
  } catch (error) {
    console.error('Error fetching archived products:', error);
    return res.status(500).json({ message: 'Internal Server Error' });
  }
};


