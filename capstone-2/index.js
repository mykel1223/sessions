// [SECTION] Dependencies and Modules
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

require('dotenv').config();


const userRoutes = require("./routes/User.js");
const productRoutes = require("./routes/productRoutes.js");
// Allows access to routes defined within our application


// [SECTION] Environment Setup
const port = 4000;

// [SECTION] Server Setup

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Allows all resources to access our backend application
app.use(cors()); 

// [SECTION] Database Connection
mongoose.connect("mongodb+srv://admin:admin123@cluster0.reijznp.mongodb.net/CAPSTONE2?retryWrites=true&w=majority", {
	useNewUrlParser : true,
	useUnifiedTopology : true
	// allows us to avoid any current or future errors while connecting to mongoDB
});

mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas!'));


// [SECTION] Backend Routes
app.use("/user", userRoutes);
app.use("/product", productRoutes);


// [SECTION] Server Gateway Response

if(require.main === module){
	app.listen(process.env.PORT || port, () => {
		console.log(`API is now online on port ${process.env.PORT || port}`)
	});
};

module.exports = app;