const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	firstName : {
		type : String,
		required : [true, "First name is required!"]
	},
	userOrders : [
		{
			productId : {
				type : String,
				required : [true, "Product ID is required!"]
			},
			purchasedOn : {
				type : Date,
				default : new Date()
			},
			status : {
				type : String,
				default : "Already in the cart"
			},
			price : {
				type : Number,
				required : [true, "Product ID is required!"]
			}
		}
	]
	lastName : {
		type : String,
		required : [true, "Last name is required!"]
	},
	email : {
		type : String,
		required : [true, "Email is required!"]
	},
	password : {
		type : String,
		required : [true, "Password is required!"]
	},
	isAdmin : {
		type : Boolean,
	 default : false
	},
	mobileNo : {
		type : String,
		required : [true, "Mobile number is required"]
	},
	


})


module.exports = mongoose.model("Order", orderSchema);