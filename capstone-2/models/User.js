const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		required : [true, "First name is required!"]
	},
	lastName : {
		type : String,
		required : [true, "Last name is required!"]
	},
	email : {
		type : String,
		required : [true, "Email is required!"]
	},
	password : {
		type : String,
		required : [true, "Password is required!"]
	},
	isAdmin : {
		type : Boolean,
	 default : false
	},
	mobileNo : {
		type : String,
		required : [true, "Mobile number is required"]
	},
	userOrders : [
		{
			productId : {
				type: String,
				required : [true, "Product ID is required"]			
			},
			brandName : {
				type : String
			},
			brandModel : {
				type : String
			},
			description : {
				type : String
			},
			color : {
				type : String
			},
			price : {
				type : Number
			},
			OrderedOn : {
				type : Date,
				default : new Date()
			},
			status : {
				type : String,
				default : "Already in the cart"
			}
			
			
		}
	]


})


module.exports = mongoose.model("User", userSchema);