// [SECTION] Dependencies and Modules
const mongoose = require("mongoose");

// [SECTION] Schema/Blueprint
const productSchema = new mongoose.Schema({
	brandName : {
		type : String,
		required : [true, "Course is required!"]
	},
	brandModel : {
		type : String,
		required : [true, "Brand is required!"]
	},
	description : {
		type : String,
		required : [true, "Description is required!"]
	},
	img : {
		type : String,
		required : [true, "Image is required!"]
	},
	color : {
		type : String,
		required : [true, "Color is required!"]
	},
	quantity : {
		type : Number,
		default : "1"
	},
	price : {
		type : Number,
		required : [true, "Price is required!"]
	},
	isActive : {
		type : Boolean,
	 default : true
	},
	createdOn : {
		type : Date,
		// The "new Date()" expression that instantiates a new "date" that store the current time and date whenever a course is created in our database
		default : new Date()
	},
	orders : [
		{
			userId : {
				type : String,
				required : [true, "UserId is required!"]
			},
			purchasedOn : {
				type : Date,
				default : new Date()
			}
		}
	]
});
 
// [SECTION] Model
module.exports = mongoose.model("Product", productSchema);
